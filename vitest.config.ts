import { defineConfig } from "vite";

export default defineConfig({
  test: {
    coverage: {
      provider: 'v8',
      all: true,
      reporter: ['cobertura', 'text']
    }
  }
});
