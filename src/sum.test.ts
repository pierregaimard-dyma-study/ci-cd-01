import { expect, test } from "vitest";
import {sum} from "./sum.ts";

test('1 + 1 = 2', () => {
  expect(sum(1, 1)).toEqual(2);
});